## Inhoud van de voorpagina

Deze website wordt door aangedreven door [GitLab Pages](https://about.gitlab.com/features/pages/)
/ [Hugo](https://gohugo.io) en kan binnen een minuut worden gebouwd.
Letterlijk.
De paginas gebruiken het `beautifulhugo` thema welke de inhoud van de voorpagina ondersteund.
Bewerk `/content/_index.md` om wat hier wordt getoond to veranderen. verwijder `/content/_index.md`
als je hier geen inhoud wilt hebben.

Ga naar [GitLab project](https://gitlab.com/pages/hugo) om te beginnen.
